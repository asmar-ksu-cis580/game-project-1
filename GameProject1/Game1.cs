﻿using System.Collections.Generic;
using System.Diagnostics;
using GameProject1.Entities;
using GameProject1.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameProject1;

public class Game1 : Game
{
    private GraphicsDeviceManager _graphics;
    private SpriteBatch _spriteBatch;

    private Puck _puck;
    private List<Comet> _comets = new List<Comet>();

    public (Vector2, Vector2) GetNewCometParameters()
    {
        var viewport = _graphics.GraphicsDevice.Viewport;
        var position = Vector2Helper.Random();
        position.X = position.X * (viewport.Width - Comet.RenderWidth) + Comet.RenderWidth / 2;
        position.Y = position.Y * (viewport.Height - Comet.RenderHeight) + Comet.RenderHeight / 2;
        var direction = Vector2Helper.Random();
        direction.Normalize();
        return (direction, position);
    }

    public Game1()
    {
        _graphics = new GraphicsDeviceManager(this);
        Content.RootDirectory = "Content";
        IsMouseVisible = true;
    }

    protected override void Initialize()
    {
        _graphics.IsFullScreen = true;
        _graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
        _graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
        _graphics.PreferMultiSampling = true;
        _graphics.GraphicsProfile = GraphicsProfile.HiDef;
        _graphics.GraphicsDevice.PresentationParameters.MultiSampleCount = 8;
        _graphics.ApplyChanges();

        var viewport = _graphics.GraphicsDevice.Viewport;

        {
            var position = Vector2Helper.Random();
            position.X = position.X * (viewport.Width - Puck.RenderResolution) + Puck.RenderResolution / 2;
            position.Y = position.Y * (viewport.Height - Puck.RenderResolution) + Puck.RenderResolution / 2;
            _puck = new Puck(Vector2.Zero, position);
        }
        for (int i = 0; i < 2; ++i)
        {
            var (direction, position) = GetNewCometParameters();
            _comets.Add(new Comet(direction, position));
        }

        base.Initialize();
    }

    protected override void LoadContent()
    {
        _spriteBatch = new SpriteBatch(GraphicsDevice);

        _puck.LoadContent(Content);
        foreach (var comet in _comets)
        {
            comet.LoadContent(Content);
        }
    }

    protected override void Update(GameTime gameTime)
    {
        if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
            Keyboard.GetState().IsKeyDown(Keys.Escape))
            Exit();

        var mouseState = Mouse.GetState();
        var viewport = _graphics.GraphicsDevice.Viewport;

        var updateContext = new UpdateContext
        {
            GameTime = gameTime,
            GraphicsDevice = _graphics.GraphicsDevice,
        };

        {
            var newDirection = mouseState.Position.ToVector2() - _puck.Center;
            newDirection.X /= viewport.Width;
            newDirection.Y /= viewport.Height;
            newDirection *= 15;
            Debug.WriteLine(newDirection);
            _puck.Direction = newDirection;
            _puck.Update(updateContext);
        }
        foreach (var comet in _comets)
        {
            comet.Update(updateContext);
        }

        foreach (var comet in _comets)
        {
            if (_puck.Bounds.CollidesWith(comet.Bounds))
            {
                var (direction, position) = GetNewCometParameters();
                comet.Direction = direction;
                comet.Position = position;
            }
        }

        base.Update(gameTime);
    }

    protected override void Draw(GameTime gameTime)
    {
        GraphicsDevice.Clear(Color.CornflowerBlue);

        var drawingContext = new DrawingContext
        {
            SpriteBatch = _spriteBatch,
        };

        _spriteBatch.Begin();

        _puck.Draw(drawingContext);

        foreach (var comet in _comets)
        {
            comet.Draw(drawingContext);
        }

        _spriteBatch.End();

        base.Draw(gameTime);
    }
}