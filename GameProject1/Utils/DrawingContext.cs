﻿using Microsoft.Xna.Framework.Graphics;

namespace GameProject1.Utils
{
    public struct DrawingContext
    {
        public SpriteBatch SpriteBatch { get; init; }
    }
}