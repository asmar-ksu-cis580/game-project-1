﻿using System;
using Microsoft.Xna.Framework;

namespace GameProject1.Utils;

public class Vector2Helper
{
    public static Random RandomGen = new Random();

    /// <summary>
    /// Generate a random Vector2.
    /// </summary>
    /// <returns>random Vector2</returns>
    public static Vector2 Random()
    {
        return new Vector2((float)RandomGen.NextDouble(), (float)RandomGen.NextDouble());
    }
}