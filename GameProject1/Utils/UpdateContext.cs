﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameProject1.Utils;

public struct UpdateContext
{
    public GameTime GameTime { get; init; }
    public GraphicsDevice GraphicsDevice { get; init; }
}