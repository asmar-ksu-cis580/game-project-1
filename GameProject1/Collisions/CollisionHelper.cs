﻿using System;
using Microsoft.Xna.Framework;

namespace GameProject1.Collisions
{
    public static class CollisionHelper
    {
        /// <summary>
        /// Detects a collision between two BoundingCircles.
        /// </summary>
        /// <param name="a">The first bounding circle.</param>
        /// <param name="b">The second bounding circle.</param>
        /// <returns>true if circles collide, false otherwise</returns>
        public static bool Collide(BoundingCircle a, BoundingCircle b)
        {
            return Math.Pow(a.Radius + b.Radius, 2) >=
                   Math.Pow(a.Center.X - b.Center.X, 2) + Math.Pow(a.Center.Y - b.Center.Y, 2);
        }

        /// <summary>
        /// Detects a collision between two BoundingRectangles.
        /// </summary>
        /// <param name="a">The first rectangle.</param>
        /// <param name="b">The second rectangle.</param>
        /// <returns>true if rectangles collide, false otherwise</returns>
        public static bool Collide(BoundingRectangle a, BoundingRectangle b)
        {
            return a.Right >= b.Left && a.Left <= b.Right && a.Top <= b.Bottom && a.Bottom >= b.Top;
        }

        /// <summary>
        /// Detects a collision between a BoundingCircle and a BoundingRectangle.
        /// </summary>
        /// <param name="circle">The circle.</param>
        /// <param name="rectangle">The rectangle.</param>
        /// <returns>true if they collide, false otherwise</returns>
        public static bool Collide(BoundingCircle circle, BoundingRectangle rectangle)
        {
            float nearestX = MathHelper.Clamp(circle.Center.X, rectangle.Left, rectangle.Right);
            float nearestY = MathHelper.Clamp(circle.Center.Y, rectangle.Top, rectangle.Bottom);
            return Math.Pow(circle.Radius, 2) >=
                   Math.Pow(circle.Center.X - nearestX, 2) + Math.Pow(circle.Center.Y - nearestY, 2);
        }

        /// <summary>
        /// Detects a collision between a BoundingRectangle and a BoundingCircle.
        /// </summary>
        /// <param name="rectangle">The rectangle.</param>
        /// <param name="circle">The circle.</param>
        /// <returns>true if they collide, false otherwise</returns>
        public static bool Collide(BoundingRectangle rectangle, BoundingCircle circle) => Collide(circle, rectangle);
    }
}